use std::process::Command;
use std::env;
use std::path::Path;

fn main () {
    let dst = env::var("OUT_DIR").unwrap();

    Command::new("gcc")
        .args(&["src/callme.c", "-Og", "-ggdb",
                "-Wall", "-Werror", "-Wfatal-errors",
                "-c", "-fPIC", "-o"])
        .arg(&format!("{}/callme.o", dst))
        .status().unwrap();
    Command::new("ar").args(&["crs", "libcallme.a", "callme.o"])
                      .current_dir(&Path::new(&dst))
                      .status().unwrap();

    println!("cargo:rustc-link-lib=static=callme");
    println!("cargo:rustc-link-search=native={}", dst);
}
