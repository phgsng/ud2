/*
https://doc.rust-lang.org/nomicon/ffi.html#the-nullable-pointer-optimization
*/
#[allow(non_camel_case_types)]
type cb_fun = Option<unsafe extern "C" fn(n: u32) -> u32>;

extern "C" {
    fn callme (cb: cb_fun, arg: u32) -> u32;
    fn c_cb (n: u32) -> u32;
}

fn main () {
    let cb0: cb_fun = None;
    let cbc: cb_fun = Some(c_cb);
    let cbr: cb_fun = Some(callback);

    let res = unsafe { callme (cb0, 42) };
    eprintln! ("got {}", res);

    let res = unsafe { callme (cbc, 42) };
    eprintln! ("got {}", res);

    let res = unsafe { callme (cbr, 42) };
    eprintln! ("got {}", res);
}

extern "C"
fn callback (n: u32) -> u32 {
    eprintln! ("called with {}", n);

    1337 * n
}
