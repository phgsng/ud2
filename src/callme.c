#include "callme.h"

extern uint32_t callme (cb_fun cb, uint32_t arg)
{
    if (cb == NULL)
        return 0x1701d;

    return (*cb) (arg);
}

extern uint32_t c_cb (uint32_t n)
{
    return n * n;
}
