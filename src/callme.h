#ifndef CALLME_H
#define CALLME_H
#include <stdint.h>
#include <stdlib.h>
typedef uint32_t (*cb_fun) (uint32_t);
uint32_t callme (cb_fun, uint32_t);
uint32_t c_cb (uint32_t);
#endif /* [CALLME_H] */
