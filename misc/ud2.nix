with import <nixpkgs> {};

stdenv.mkDerivation rec {
  name = "ud2-env";
  buildInputs =
    [ pkgconfig
      llvmPackages.libclang
      clang
      gcc
    ];

  LIBCLANG_PATH="${llvmPackages.libclang}/lib";
}
